//
//  SATScoreService.swift
//  NYCSchools
//
//  Created by Rameez Khan on 8/29/23.
//

import Foundation


import Foundation

protocol SATScoreServiceProtocol : AnyObject {
    func fetchScore(for school: String, _ completion: @escaping ((Result<[Score], ErrorResult>) -> Void))
}

final class SATScoreService: SATScoreServiceProtocol {
    
    static let shared = SATScoreService()
    var task : URLSessionTask?
    
    /// This function fetches the score of a school  from the API
    func fetchScore(for school: String, _ completion: @escaping ((Result<[Score], ErrorResult>) -> Void)) {
        let baseUrlString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        let queryString = "?dbn=\(school)"
        
        let urlString = baseUrlString + queryString
        
        // cancel previous request if already in progress
        self.cancelFetchScore()
        
        task = Service().dataTask(urlString: urlString, completion: { result in
            DispatchQueue.global(qos: .background).async(execute: {
                switch result {
                case .success(let data):
                    do {
                        let decoder = JSONDecoder()
                        let scoreData = try decoder.decode([Score].self, from: data)
                        completion(.success(scoreData))
                    } catch {
                        completion(.failure(.decoder(string: "Unable to decode data: \(error.localizedDescription)")))
                    }
                case .failure(let error):
                    completion(.failure(.network(string: "Network error " + error.localizedDescription)))
                }
            })
        })
    }
    
    func cancelFetchScore() {
        if let task = task {
            task.cancel()
        }
        task = nil
    }
}
