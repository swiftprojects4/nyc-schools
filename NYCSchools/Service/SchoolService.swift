//
//  SchoolService.swift
//  NYCSchools
//
//  Created by Rameez Khan on 8/29/23.
//

import Foundation

protocol SchoolServiceProtocol: AnyObject {
    func fetchSchools(_ completion: @escaping ((Result<[School], ErrorResult>) -> Void))
}

final class SchoolService: SchoolServiceProtocol {
    
    static let shared = SchoolService()
    var task : URLSessionTask?
    
    /// This function fetches list of schools from the API
    func fetchSchools(_ completion: @escaping ((Result<[School], ErrorResult>) -> Void)) {
        let urlString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        
        // cancel previous request if already in progress
        self.cancelFetchSchools()
        
        task = Service().dataTask(urlString: urlString, completion: { result in
            DispatchQueue.global(qos: .background).async(execute: {
                switch result {
                case .success(let data):
                    do {
                        let decoder = JSONDecoder()
                        let schoolData = try decoder.decode([School].self, from: data)
                        completion(.success(schoolData))
                    } catch {
                        completion(.failure(.decoder(string: "Unable to decode data: \(error.localizedDescription)")))
                    }
                case .failure(let error):
                    completion(.failure(.network(string: "Network error " + error.localizedDescription)))
                }
            })
        })
    }
    
    func cancelFetchSchools() {
        if let task = task {
            task.cancel()
        }
        task = nil
    }
}
