//
//  School.swift
//  NYCSchools
//
//  Created by Rameez Khan on 8/29/23.
//

import Foundation

struct School: Codable {
    let id: String
    let name: String?
    let grades: String?
    let phone: String?
    let email: String?
    let website: String?
    let streetAddress: String?
    let city: String?
    let state: String?
    let zip: String?
    
    private enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case grades = "finalgrades"
        case phone = "phone_number"
        case email = "school_email"
        case website = "website"
        case streetAddress = "primary_address_line_1"
        case city = "city"
        case state = "state_code"
        case zip = "zip"
    }
}
