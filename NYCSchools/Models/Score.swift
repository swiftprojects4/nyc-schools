//
//  File.swift
//  NYCSchools
//
//  Created by Rameez Khan on 8/29/23.
//

import Foundation

struct Score: Codable {
    let id: String
    let reading: String?
    let math: String?
    let writing: String?
    
    private enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case reading = "sat_critical_reading_avg_score"
        case math = "sat_math_avg_score"
        case writing = "sat_writing_avg_score"
    }
}
