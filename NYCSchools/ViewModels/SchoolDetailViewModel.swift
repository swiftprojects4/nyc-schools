//
//  SchoolDetailViewModel.swift
//  NYCSchools
//
//  Created by Rameez Khan on 8/29/23.
//

import Foundation

struct SchoolDetailViewModel {
    
    // MARK: - Properties

    weak var scoreService: SATScoreServiceProtocol?
    weak var score: GenericData<Score>?
    
    var school: School?
    
    var onErrorHandling : ((ErrorResult?) -> Void)?
    
    // MARK: - Init
    init(scoreService: SATScoreServiceProtocol = SATScoreService.shared, score: GenericData<Score>? = nil, school: School? = nil) {
        self.scoreService = scoreService
        self.score = score
        self.school = school
    }
    
    // MARK: - Public Methods
    func fetchScores(for schoolID: String?) {
        guard let service = scoreService else {
            onErrorHandling?(ErrorResult.custom(string: "Missing score service!"))
            return
        }
        guard let schoolID = schoolID else {
            onErrorHandling?(ErrorResult.custom(string: "Can't fetch scores without School DB!"))
            return
        }
        service.fetchScore(for: schoolID) { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let score):
                    self.score?.data.value = score
                case .failure(let error):
                    self.onErrorHandling?(error)
                }
            }
        }
    }

}
