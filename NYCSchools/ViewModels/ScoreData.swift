//
//  ScoreData.swift
//  NYCSchools
//
//  Created by Rameez Khan on 8/30/23.
//

import Foundation

class GenericData<T>: NSObject {
    var data: DynamicValue<[T]> = DynamicValue([])
}

class ScoreData: GenericData<Score> {
}
