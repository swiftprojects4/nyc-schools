//
//  SchoolDataSource.swift
//  NYCSchools
//
//  Created by Rameez Khan on 8/29/23.
//

import Foundation
import UIKit

class GenericDataSource<T> : NSObject {
    var data: DynamicValue<[T]> = DynamicValue([])
}

class SchoolDataSource : GenericDataSource<School>, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "schoolCellIdentifier", for: indexPath) as! SchoolTableViewCell
        
        let school = self.data.value[indexPath.row]
        cell.schoolName = school.name
        
        return cell
    }
}
