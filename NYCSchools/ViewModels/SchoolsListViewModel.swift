//
//  SchoolsListViewModel.swift
//  NYCSchools
//
//  Created by Rameez Khan on 8/29/23.
//

import Foundation

struct SchoolsListViewModel {
    
    // MARK: - Properties
    weak var service: SchoolServiceProtocol?
    weak var dataSource: GenericDataSource<School>?
    
    var onErrorHandling : ((ErrorResult?) -> Void)?
    
    var detailViewModel: SchoolDetailViewModel {
        return SchoolDetailViewModel()
    }

    // MARK: - Init
    init(service: SchoolServiceProtocol = SchoolService.shared, dataSource: GenericDataSource<School>) {
        self.service = service
        self.dataSource = dataSource
    }
    
    // MARK: - Public Methods
    func fetch() {
        guard let service = service else {
            onErrorHandling?(ErrorResult.custom(string: "Missing school service!"))
            return
        }
        service.fetchSchools { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let schools):
                    self.dataSource?.data.value = schools
                case .failure(let error):
                    self.onErrorHandling?(error)
                }
            }
        }

    }
}
