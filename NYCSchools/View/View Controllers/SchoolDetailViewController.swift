//
//  SchoolDetailViewController.swift
//  NYCSchools
//
//  Created by Rameez Khan on 8/29/23.
//

import UIKit

class SchoolDetailViewController: UIViewController {
        
    // MARK: - IBOutlets
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var gradesLabel: UILabel!
    
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    

    @IBOutlet weak var phoneNumLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var streetAddrLabel: UILabel!
    @IBOutlet weak var cityStateZipLabel: UILabel!
    
    // MARK: - Properties
    let scoreData = ScoreData()
    
    lazy var viewModel: SchoolDetailViewModel = {
        let viewModel = SchoolDetailViewModel(score: scoreData)
        return viewModel
    }()
    
    var school: School?
    
    // MARK: - Public Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        scoreData.data.addAndNotify(observer: self) { [weak self] scores in
            if let score = scores.first,
               let readingScore = score.reading,
               let mathScore = score.math,
               let writingScore = score.writing {
                self?.readingScoreLabel.text = "Reading: " + readingScore
                self?.mathScoreLabel.text = "Math: " + mathScore
                self?.writingScoreLabel.text = "Writing: " + writingScore
            }
        }
        
        //  error handling
        self.viewModel.onErrorHandling = { [weak self] error in
            // display error
            print("Error is ", error.debugDescription)
            let controller = UIAlertController(title: "An error occured", message: "Oops, something went wrong!", preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
            self?.present(controller, animated: true, completion: nil)
        }
        viewModel.fetchScores(for: school?.id)
    }
    
    private func setupUI() {
        let grades = school?.grades ?? "NA"
        let city = school?.city ?? "NA"
        let state = school?.state ?? ""
        let zip = school?.zip ?? ""

        schoolNameLabel.text = school?.name
        gradesLabel.text = "Grades: " + grades
        phoneNumLabel.text = school?.phone
        emailLabel.text = school?.email
        websiteLabel.text = school?.website
        streetAddrLabel.text = school?.streetAddress
        cityStateZipLabel.text = city + " " + state + " " + zip
        
    }
   

}
