//
//  SchoolsListViewController.swift
//  NYCSchools
//
//  Created by Rameez Khan on 8/29/23.
//

import UIKit

class SchoolsListViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    let dataSource = SchoolDataSource()
    
    lazy var viewModel: SchoolsListViewModel = {
        let viewModel = SchoolsListViewModel(dataSource: dataSource)
        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }
    
    private func setup() {
        self.title = "NYC Schools List"

        tableView.dataSource = self.dataSource
        tableView.delegate = self
        dataSource.data.addAndNotify(observer: self) { [weak self] _ in
            self?.tableView.reloadData()
        }
        
        //  error handling
        self.viewModel.onErrorHandling = { [weak self] error in
            // display error
            print("Error is ", error.debugDescription)
            let controller = UIAlertController(title: "An error occured", message: "Oops, something went wrong!", preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
            self?.present(controller, animated: true, completion: nil)
        }
        
        self.viewModel.fetch()
    }

}

extension SchoolsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "SchoolDetailViewController") as? SchoolDetailViewController {
            let selectedSchool = dataSource.data.value[indexPath.row]
            detailVC.school = selectedSchool
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
        
    }
    
}
