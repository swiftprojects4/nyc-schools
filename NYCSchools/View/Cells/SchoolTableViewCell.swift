//
//  SchoolTableViewCell.swift
//  NYCSchools
//
//  Created by Rameez Khan on 8/29/23.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var schoolNameLabel: UILabel!
    
    // MARK: - Properties
    var schoolName: String? {
        didSet {
            guard let schoolName = schoolName else {
                return
            }
            schoolNameLabel.text = schoolName
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
