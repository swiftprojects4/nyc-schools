//
//  SchoolDataSourceTests.swift
//  NYCSchoolsTests
//
//  Created by Rameez Khan on 8/30/23.
//

import XCTest
@testable import NYCSchools

final class SchoolDataSourceTests: XCTestCase {
    
    var dataSource: SchoolDataSource!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        dataSource = SchoolDataSource()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        dataSource = nil
    }

    func testEmptyValueInDataSource() throws {
        
        // giving empty data value
        dataSource.data.value = []
        
        let tableView = UITableView()
        tableView.dataSource = dataSource
        
        // expected one section
        XCTAssertEqual(dataSource.numberOfSections(in: tableView), 1, "Expected one section in table view")
        
        // expected zero cells
        XCTAssertEqual(dataSource.tableView(tableView, numberOfRowsInSection: 0), 0, "Expected no cell in table view")
    }
    
    func testValueInDataSource() throws {
        
        // giving data value
        let school1 = School(id: "ABC123", name: "KINGSBOROUGH EARLY COLLEGE SCHOOL", grades: "9-12", phone: "987-654-0000", email: "abc@kb.org", website: "www.KECSS.info", streetAddress: "2630 Benson Avenue", city: "Brooklyn", state: "NY", zip: "11214")
        let school2 = School(id: "ABC124", name: "Urban Assembly New York Harbor School", grades: "6-12", phone: "345-654-0000", email: "abc@nyharbour.org", website: "www.newyorkharborschool.org", streetAddress: "10 South Street Slip 7", city: "Manhattan", state: "NY", zip: "10004")

        dataSource.data.value = [school1, school2]
        
        let tableView = UITableView()
        tableView.dataSource = dataSource
        
        // expected one section
        XCTAssertEqual(dataSource.numberOfSections(in: tableView), 1, "Expected one section in table view")
        
        // expected two cells
        XCTAssertEqual(dataSource.tableView(tableView, numberOfRowsInSection: 0), 2, "Expected two cell in table view")
    }

}
