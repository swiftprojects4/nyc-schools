//
//  SchoolServiceTests.swift
//  NYCSchoolsTests
//
//  Created by Rameez Khan on 8/30/23.
//

import XCTest
@testable import NYCSchools

final class SchoolServiceTests: XCTestCase {

    func testCanceRequest() throws {
        // making sure it has a previous session
        SchoolService.shared.fetchSchools { _ in
            // ignore the call
        }
        
        // After calling cancel function, expect the task to be nil
        SchoolService.shared.cancelFetchSchools()
        XCTAssertNil(SchoolService.shared.task, "Expected task nil")
    }

}
