//
//  SchoolListViewModelTests.swift
//  NYCSchoolsTests
//
//  Created by Rameez Khan on 8/30/23.
//

import XCTest
@testable import NYCSchools

final class SchoolListViewModelTests: XCTestCase {
    
    var viewModel: SchoolsListViewModel!
    var dataSource: GenericDataSource<School>!
    fileprivate var service: MockSchoolService!

    override func setUpWithError() throws {
        self.service = MockSchoolService()
        self.dataSource = GenericDataSource<School>()
        self.viewModel = SchoolsListViewModel(service: service, dataSource: dataSource)
    }

    override func tearDownWithError() throws {
        self.viewModel = nil
        self.dataSource = nil
        self.service = nil
    }

    func testFetchWithNoService() throws {
        
        let expectation = XCTestExpectation(description: "No service school")
        
        // giving no service to a view model
        viewModel.service = nil
        
        // expected to not be able to fetch currencies
        viewModel.onErrorHandling = { error in
            expectation.fulfill()
        }
        
        viewModel.fetch()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testFetch() throws {
        
        let expectation = XCTestExpectation(description: "Fetch schools")
        
        // mock schools to a service
        let school1 = School(id: "ABC123", name: "KINGSBOROUGH EARLY COLLEGE SCHOOL", grades: "9-12", phone: "987-654-0000", email: "abc@kb.org", website: "www.KECSS.info", streetAddress: "2630 Benson Avenue", city: "Brooklyn", state: "NY", zip: "11214")
        let school2 = School(id: "ABC124", name: "Urban Assembly New York Harbor School", grades: "6-12", phone: "345-654-0000", email: "abc@nyharbour.org", website: "www.newyorkharborschool.org", streetAddress: "10 South Street Slip 7", city: "Manhattan", state: "NY", zip: "10004")

        service.schools = [school1, school2]
        
        viewModel.onErrorHandling = { _ in
            XCTAssert(false, "ViewModel should not be able to fetch without service")
        }
        
        dataSource.data.addObserver(self) { _ in
            expectation.fulfill()
        }
        
        viewModel.fetch()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testFetchNoSchools() throws {
        
        let expectation = XCTestExpectation(description: "No school")
        
        // mocking nil to a service to trigger error
        service.schools = nil
        
        // expected completion to fail
        viewModel.onErrorHandling = { error in
            expectation.fulfill()
        }
        
        viewModel.fetch()
        wait(for: [expectation], timeout: 5.0)
    }
}

fileprivate class MockSchoolService: SchoolServiceProtocol {
    
    var schools: [School]?

    func fetchSchools(_ completion: @escaping ((Result<[School], ErrorResult>) -> Void)) {

        if let schools = schools {
            completion(Result.success(schools))
        } else {
            completion(Result.failure(ErrorResult.custom(string: "No schools fetched")))
        }
    }
}
